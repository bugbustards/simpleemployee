package org.bugbustards.simpleemployee.client;

import org.bugbustards.simpleemployee.shared.domain.Employee;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 27.11.2014
 *         Time: 3:49
 */
public interface EditListener {
    void edit(Employee employee);
}
