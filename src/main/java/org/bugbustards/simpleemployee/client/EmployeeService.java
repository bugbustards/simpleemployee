package org.bugbustards.simpleemployee.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.List;
import org.bugbustards.simpleemployee.shared.domain.Employee;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 3:26
 */
@RemoteServiceRelativePath("springGwtServices/employeeService")
public interface EmployeeService extends RemoteService {
    List<Employee> findEmployee(EmployeeFilterData employee);

    /**
     *
     * @param employee
     * @return true if someting is done, false if nor
     */
    Boolean saveEmployee(Employee employee);
    Boolean removeEmployee(int employeeId);

}
