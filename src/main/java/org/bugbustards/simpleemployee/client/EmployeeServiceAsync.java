package org.bugbustards.simpleemployee.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import org.bugbustards.simpleemployee.shared.domain.Employee;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 3:26
 */
public interface EmployeeServiceAsync {
    //todo как вариант принимать нул или сделать отлеьный метод для выбора списка
    void findEmployee(EmployeeFilterData employee, AsyncCallback<List<Employee>> callback);
    void saveEmployee(Employee employee, AsyncCallback<Boolean> callback);
    void removeEmployee(int employeeId, AsyncCallback<Boolean> callback);
}
