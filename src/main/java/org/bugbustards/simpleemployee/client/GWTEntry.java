package org.bugbustards.simpleemployee.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import java.util.List;
import org.bugbustards.simpleemployee.client.widgets.EditEmployeeWidget;
import org.bugbustards.simpleemployee.client.widgets.EmployeesListWidget;
import org.bugbustards.simpleemployee.shared.domain.Employee;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 1:02
 */
public class GWTEntry implements EntryPoint {
    private EmployeeServiceAsync employeeService = GWT.create(EmployeeService.class);

    @Override
    public void onModuleLoad() {
        final EditEmployeeWidget editEmployeeWidget = GWT.create(EditEmployeeWidget.class);
        final EmployeesListWidget employeesListWidget = GWT.create(EmployeesListWidget.class);
        final RootPanel rootPanel = RootPanel.get();

        editEmployeeWidget.setVisible(false);
        employeesListWidget.setVisible(false);

        rootPanel.add(editEmployeeWidget);
        rootPanel.add(employeesListWidget);

        employeesListWidget.setOnEditHandler(new EditListener() {
            @Override
            public void edit(Employee employee) {
                editEmployeeWidget.setEmployee(employee);
                editEmployeeWidget.setVisible(true);
            }
        });

        editEmployeeWidget.setOnCloseHandler(new Runnable() {
            @Override
            public void run() {
                employeesListWidget.setVisible(true);
                employeesListWidget.fetch();
            }
        });

        employeeService.findEmployee(EmployeeFilterData.create(), new AsyncCallback<List<Employee>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            @Override
            public void onSuccess(List<Employee> result) {
                if (result.isEmpty()) {
                    editEmployeeWidget.setVisible(true);
                    editEmployeeWidget.setEmployee(new Employee(0));

                } else {
                    employeesListWidget.setVisible(true);
                    employeesListWidget.reset(result);
                }
            }
        });

    }
}
