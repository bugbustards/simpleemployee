package org.bugbustards.simpleemployee.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import org.bugbustards.simpleemployee.client.EmployeeService;
import org.bugbustards.simpleemployee.client.EmployeeServiceAsync;
import org.bugbustards.simpleemployee.shared.domain.Employee;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 19:23
 */
public class EditEmployeeWidget extends Composite {

    interface EditEmployeeWidgetUiBinder extends UiBinder<Widget, EditEmployeeWidget> {

    }
    private static EditEmployeeWidgetUiBinder uiBinder = GWT.create(EditEmployeeWidgetUiBinder.class);

    private Runnable onCloseHandler = new Runnable() {
        @Override
        public void run() {}
    };

    private EmployeeServiceAsync employeeService = GWT.create(EmployeeService.class);

    @UiField TextBox firstName;
    @UiField TextBox secondName;
    @UiField TextBox lastName;
    @UiField TextBox age;
    @UiField TextBox experience;
    @UiField TextBox description;

    @UiField Button saveEmployee;
    @UiField Button cancelEdit;

    private Employee employee = new Employee(0);

    public EditEmployeeWidget() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        firstName.setValue(employee.getFirstName());
        secondName.setValue(employee.getSecondName());
        lastName.setValue(employee.getLastName());
        age.setValue(employee.getAge() + 0 + "");
        experience.setValue(employee.getExperience() + "");
        description.setValue(employee.getDescription());
    }

    public void setOnCloseHandler(Runnable onCloseHandler) {
        this.onCloseHandler = onCloseHandler;
    }

    @UiHandler("saveEmployee")
    void handleSaveClick(ClickEvent clickEvent) {
        employee.setFirstName(firstName.getValue());
        employee.setSecondName(secondName.getValue());
        employee.setLastName(lastName.getValue());
        employee.setAge(Integer.parseInt(age.getValue()));
        employee.setExperience(Integer.parseInt(experience.getValue()));
        employee.setDescription(description.getValue());
        final EditEmployeeWidget self = this;
        employeeService.saveEmployee(employee, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            @Override
            public void onSuccess(Boolean somethingIsDone) {
                if (somethingIsDone) {
//                    Window.alert("Изменения были сохранены");
                    self.setVisible(false);
                    onCloseHandler.run();
                } else {
                    Window.alert("Изменения не были сохранены");
                }
            }
        });
    }

    @Override
    protected void onDetach() {
        super.onDetach();
        onCloseHandler.run();
    }

    @UiHandler("cancelEdit")
    void handleCancelClick(ClickEvent clickEvent) {
        setVisible(false);
        onCloseHandler.run();
    }


}