package org.bugbustards.simpleemployee.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import java.util.List;
import org.bugbustards.simpleemployee.client.EditListener;
import org.bugbustards.simpleemployee.client.EmployeeService;
import org.bugbustards.simpleemployee.client.EmployeeServiceAsync;
import org.bugbustards.simpleemployee.shared.domain.Employee;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 19:23
 */
public class EmployeesListWidget extends Composite {

    interface EmployeesListWidgetUiBinder extends UiBinder<Widget, EmployeesListWidget> {
    }

    private EditListener onEditHandler;

    private static EmployeesListWidgetUiBinder uiBinder = GWT.create(EmployeesListWidgetUiBinder.class);

    private EmployeeServiceAsync employeeService = GWT.create(EmployeeService.class);

    @UiField Button newEmployee;
    @UiField(provided = true) Grid employeesTable;

    public EmployeesListWidget() {
        employeesTable = prepareTable(employeesTable);
        initWidget(uiBinder.createAndBindUi(this));
    }

    public void setOnEditHandler(EditListener onEditHandler) {
        this.onEditHandler = onEditHandler;
    }

    private Grid prepareTable(Grid employeesTable) {
        if (employeesTable == null) employeesTable = new Grid(1,6);
        employeesTable.setTitle("Сотрудники");
        employeesTable.setText(0, 0, "ФИО");
        employeesTable.setText(0, 1, "возраст");
        employeesTable.setText(0, 2, "опыт");
        employeesTable.setText(0, 3, "примечание");
        employeesTable.setText(0, 4, "действия");
        return employeesTable;
    }

    @UiHandler("newEmployee")
    void handleAddButtonClick(ClickEvent clickEvent) {
        setVisible(false);
        onEditHandler.edit(new Employee(0));
    }

    public void fetch() {
        employeeService.findEmployee(EmployeeFilterData.create(), new AsyncCallback<List<Employee>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            @Override
            public void onSuccess(List<Employee> result) {
                employeesTable.resizeRows(1);
                reset(result);
            }
        });
    }

    public void reset(List<Employee> employees) {
        final EmployeesListWidget self = this;
        for (final Employee employee : employees) {
            int row = employeesTable.insertRow(employeesTable.getRowCount());
            employeesTable.setText(row,0, employee.getLastName() + " " + employee.getFirstName() + " " + employee.getSecondName());
            employeesTable.setText(row,1, "" + employee.getAge());
            employeesTable.setText(row,2, "" + employee.getExperience());
            employeesTable.setText(row,3, employee.getDescription());
            employeesTable.setWidget(row, 4, new Button("изменить") {{
                this.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        self.setVisible(false);
                        onEditHandler.edit(employee);
                    }
                });

            }});
            employeesTable.setWidget(row, 5, new Button("удалить"){{
                this.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        employeeService.removeEmployee(employee.getEmployeeId(), new AsyncCallback<Boolean>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                Window.alert(caught.getMessage());
                            }

                            @Override
                            public void onSuccess(Boolean result) {
                                fetch();
                            }
                        });
                    }
                });
            }});
        }
    }

}