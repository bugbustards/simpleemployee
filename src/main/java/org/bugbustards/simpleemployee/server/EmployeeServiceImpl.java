package org.bugbustards.simpleemployee.server;

import java.util.List;
import org.bugbustards.simpleemployee.client.EmployeeService;
import org.bugbustards.simpleemployee.server.dao.EmoloyeeDAO;
import org.bugbustards.simpleemployee.shared.domain.Employee;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 3:32
 */
@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmoloyeeDAO emoloyeeDAO;

    @Override
    @Transactional
    public List<Employee> findEmployee(EmployeeFilterData employee) {
        return emoloyeeDAO.find(employee);
    }

    @Override
    @Transactional
    public Boolean saveEmployee(Employee employee) {
        return emoloyeeDAO.insertOrUpdate(employee) > 0;
    }

    @Override
    @Transactional
    public Boolean removeEmployee(int employeeId) {
        if (employeeId == 0) {
            return false;
        }
        return emoloyeeDAO.delete(employeeId) > 0;
    }
}
