package org.bugbustards.simpleemployee.server.dao;

import java.util.List;
import org.bugbustards.simpleemployee.shared.domain.Employee;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 4:46
 */
public interface EmoloyeeDAO {
    List<Employee> find(EmployeeFilterData employee);
    int insertOrUpdate(Employee employee);
    int delete(int employeeId);
}
