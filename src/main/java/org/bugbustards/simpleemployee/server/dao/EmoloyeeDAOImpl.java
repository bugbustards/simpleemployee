package org.bugbustards.simpleemployee.server.dao;

import java.util.List;
import org.bugbustards.simpleemployee.server.utils.EmployeeFilterBuilder;
import org.bugbustards.simpleemployee.shared.domain.Employee;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 4:48
 */
@Repository
public class EmoloyeeDAOImpl implements EmoloyeeDAO {

    @Qualifier("dbSessionFactory")
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<Employee> find(EmployeeFilterData filter) {
        Criteria criteria = getSession().createCriteria(Employee.class);
        for(Criterion restriction: EmployeeFilterBuilder.getCriterias(filter)) {
            criteria.add(restriction);
        }
        return criteria.list();
    }

    @Override
    @Transactional
    public int insertOrUpdate(Employee employee) {
        Session session = getSession();
        if (employee.getEmployeeId() > 0)
            session.update(employee);
        else
            session.save(employee);
        return 1;
    }

    @Override
    public int delete(int employeeId) {
        Session session = getSession();
        session.delete(new Employee(employeeId));
        return 0;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
