package org.bugbustards.simpleemployee.server.utils;

import java.util.ArrayList;
import java.util.List;
import org.bugbustards.simpleemployee.shared.domain.EmployeeFilterData;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 4:07
 */
public class EmployeeFilterBuilder {

    public static List<Criterion> getCriterias(EmployeeFilterData data) {
        List<Criterion> criterias = new ArrayList<>();
        if (data.getEmployeeId() > 0)
            criterias.add(Restrictions.eq("employeeId", data.getEmployeeId()));
        if (data.getNamePart()!=null) {
            String part = data.getNamePart();
            criterias.add(Restrictions.or(Restrictions.ilike("firstName", "%" + part + "%"),
                                                 Restrictions.ilike("lastName", "%" + part + "%"),
                                                 Restrictions.ilike("secondName", "%" + part + "%")));
        }
        if (data.getToAge() > 0)
        criterias.add(Restrictions.between("age", data.getFromAge(), data.getToAge()));

        if (data.getExperienceTo() > 0)
        criterias.add(Restrictions.between("experience", data.getExperienceFrom(), data.getExperienceTo()));
        if (data.getKeyWordPart()!= null) {
            criterias.add(Restrictions.ilike("description", "%" + data.getKeyWordPart() + "%"));
        }

        return criterias;
    }
}
