package org.bugbustards.simpleemployee.shared.domain;

import java.io.Serializable;

/**
 * @author Andrew.Arefyev@gmail.com
 *         Date: 26.11.2014
 *         Time: 4:07
 */
public class EmployeeFilterData implements Serializable {

    private String keyWordPart;
    private int experienceFrom;
    private int experienceTo;
    private int fromAge;
    private int toAge;
    private int employeeId;
    private String namePart;

    public static EmployeeFilterData create() {
        return new EmployeeFilterData();
    }

    public EmployeeFilterData setEmployeeId(int employeeId) {
        this.employeeId = employeeId;

        return this;
    }

    public EmployeeFilterData setNamePart(String namePart) {
        this.namePart = namePart;

        return this;
    }

    public EmployeeFilterData setAgeRange(int fromAge, int toAge) {
        this.fromAge = fromAge;
        this.toAge = toAge;

        return this;
    }

    public EmployeeFilterData setExperienceRange(int experienceFrom, int experienceTo) {
        this.experienceFrom = experienceFrom;
        this.experienceTo = experienceTo;

        return this;
    }

    public EmployeeFilterData setKeyWordPart(String keyWordPart) {
        this.keyWordPart = keyWordPart;

        return this;
    }

    public String getKeyWordPart() {
        return keyWordPart;
    }

    public int getExperienceFrom() {
        return experienceFrom;
    }

    public int getExperienceTo() {
        return experienceTo;
    }

    public int getFromAge() {
        return fromAge;
    }

    public int getToAge() {
        return toAge;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getNamePart() {
        return namePart;
    }
}
